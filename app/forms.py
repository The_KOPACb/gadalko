from flask_wtf import FlaskForm
from wtforms import StringField, DateField, SubmitField
from wtforms.validators import DataRequired, InputRequired


class Person(FlaskForm):
    name = StringField('Имя', validators=[DataRequired()])
    # dob = DateField('Дата рождения', format='%d/%m/%Y', validators=[InputRequired()])
    dob = DateField('Дата рождения', validators=[InputRequired()])
    # dob = DateField('Дата рождения', format='%d/%m/%Y', )
    submit = SubmitField('Гадать')
