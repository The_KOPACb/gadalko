from flask import Flask

app = Flask(__name__)
app.config['SECRET_KEY'] = 'you-will-never-guess'
app.config['MESSAGE_FLASHING_OPTIONS'] = {'duration': 5}
# app.config['STATIC_URL_PATH'] = 'astatic/'
# app.config['STATIC_FOLDER'] = 'astatic'
# app._static_folder = 'static'
from app import routes
