import re, os
from flask import render_template, flash, redirect, url_for, send_from_directory
from app import app
from app.forms import Person
from time import sleep
import secrets


#
# @app.route('/stat/<path:filename>')
# def serve_static(filename):
#     print(filename)
#     return send_from_directory('static', filename)


@app.route('/', methods=['GET', 'POST'])
def index():
    form = Person()
    # print(form)
    if form.validate_on_submit():
        print()
        z, m, me = zadiak_info(form.dob.data)
        file = open("horoscopes.csv", "r")
        predictions = []
        for word in file:
            pattern = me
            if re.search(pattern, word):
                predictions.append(word.split(",")[2].replace('"', ''))
        prediction = secrets.choice(predictions)
        print(prediction)
        flash(prediction)
        return redirect(url_for('index'))
    return render_template('index.html', form=form)



def zediac(year):
    a = {0: "крыса", 1: "бык", 2: "тигр", 3: "кролик", 4: "дракон", 5: "змея", 6: "лошадь", 7: "овца",
         8: 'обезьяна',
         9: 'курица', 10: 'собака', 11: 'свинья'}
    return a[(year - 1972) % 12]

def xinzuo(month, day):
    m = ['Водолей', 'Рыбы', 'Овен', 'Телец', 'Близнецы', 'Рак', 'Лев', 'Дева', 'Весы', "Скорпион", "Стрелец",
         "Козерог"]
    me = ['aquarius', 'pisces', 'aries', 'taurus', 'gemini', 'cancer', 'leo', 'virgo', 'libra', 'scorpio', 'sagittarius',
         "capricorn"]
    d = (20, 19, 21, 20, 21, 22, 23, 23, 23, 24, 23, 22)
    month = month - 1;
    if day > d[month]:
        return m[month], me[month]
    else:
        return m[month - 1], me[month - 1]


def zadiak_info(date):
    # print(date)
    date = str(date)
    a = date.split('-')
    # print(a)
    year, month, day = a
    year = int(year)
    month = int(month)
    day = int(day)
    # print(year, month, day)
    a, b = xinzuo(month, day)
    return zediac(year), a, b


# @app.route('/static/<path>')
# def serve_static(path):
#     print(path)
#     return send_from_directory('static', path)\

# @app.route("/static/<path:name>")
# def static_file(name):
#     return send_from_directory(
#         os.path.join(app.root_path, 'static'),
#         name,
#         # app.config['STATIC_FOLDER'], name, as_attachment=True
#     )

# @app.route("/asd")
# def static():
#     return ""
