import os, json
from flask import Flask, send_from_directory, render_template

app = Flask(
    __name__,
)

@app.route('/')
def index():
    return render_template('index.html')

# @app.route('/static/<path:filename>')
# def serve_static(filename):
#     print(filename)
#     return send_from_directory('static', filename)

if __name__ == "__main__":
    app.run(
        port=8081,
        host="0.0.0.0",
        debug=True
    )
